local memory = require("memory")

local process_player_jet_patch_address = 0x50177A
local process_control_patch_address = 0x6B192F
local process_flying_car_stuff_patch_address = 0x6CB8FB

local function enable_control_loss()
   local process_player_jet_original_code = memory.strptr("\x72\x28")
   local process_control_original_code = memory.strptr("\x7A")
   local process_flying_car_stuff_original_code = memory.strptr("\x7A")

   memory.copy(process_player_jet_patch_address, process_player_jet_original_code, 2, true)
   memory.copy(process_control_patch_address, process_control_original_code, 1, true)
   memory.copy(process_flying_car_stuff_patch_address, process_flying_car_stuff_original_code, 1, true)
end

local function disable_control_loss()
   local process_player_jet_patch = memory.strptr("\x90\x90")
   local process_control_patch = memory.strptr("\xEB")
   local process_flying_car_stuff_patch = memory.strptr("\xEB")

   memory.copy(process_player_jet_patch_address, process_player_jet_patch, 2, true)
   memory.copy(process_control_patch_address, process_control_patch, 1, true)
   memory.copy(process_flying_car_stuff_patch_address, process_flying_car_stuff_patch, 1, true)
end

disable_control_loss()